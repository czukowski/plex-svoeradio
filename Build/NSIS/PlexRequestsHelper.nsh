# Helper functions for HTTP requests to Plex.

!ifndef PLEX_REQUESTS_HELPER_INCLUDED
!define PLEX_REQUESTS_HELPER_INCLUDED

!define PLEX_CLIENT_ID       "0970c1e2-09bd-4fc5-b2f5-c0463609c698"  # Plex Client identifier.
!define PLEX_CLIENT_PLATFORM "PC"
!define PLEX_DEVICE          "PC"
!define PLEX_PLATFORM        "Windows"
!define PLEX_PRODUCT_NAME    "Nullsoft Install System"               # Plex Client Product name, will appear in devices list.

!include LogicLib.nsh
!include WinVer.nsh

# Helper function to compose HTTP headers for requests to Plex.
# Usage:
#     Call GetPlexTvAuthToken  ; Stack now has Auth Token on top.
#     Push "application/xml"   ; Add Accept type value to stack.
#     ${CallArtificialFunction} PlexTvRequestHeaders
#     Pop $0                   ; $0 now contains the headers string.
!macro PlexTvRequestHeaders
	# Stack listings below ordered from top. Registers ($0, $1,..) represent original values, ie before function call.
	# Stack: <accept> <token>, reg: $0 $1 $2 $3
	Exch $0
	# Stack: $0 <token>, reg: <accept> $1 $2 $3
	Exch
	# Stack: <token> $0, reg: <accept> $1 $2 $3
	Exch $1
	# Stack: $1 $0, reg: <accept> <token> $2 $3
	Push $2
	# Stack: $2 $1 $0, reg: <accept> <token> $2 $3
	Push $3
	# Stack: $3 $2 $1 $0, reg: <accept> <token> $2 $3
	${CallArtificialFunction} PlexDeviceName       # Get this computer name.
	Pop $2
	# Stack: $3 $2 $1 $0, reg: <accept> <token> <device> $3
	${CallArtificialFunction} PlexPlatformVersion  # Get Windows version.
	Pop $3
	# Stack: $3 $2 $1 $0, reg: <accept> <token> <device> <version>

	# Compose headers.
	StrCpy $0 "Accept: $0$\r$\n\
               X-Plex-Device: ${PLEX_DEVICE}$\r$\n\
               X-Plex-Device-Name: $2$\r$\n\
               X-Plex-Product: ${PLEX_PRODUCT_NAME}$\r$\n\
               X-Plex-Client-Identifier: ${PLEX_CLIENT_ID}$\r$\n\
               X-Plex-Client-Platform: ${PLEX_CLIENT_PLATFORM}$\r$\n\
               X-Plex-Platform: ${PLEX_PLATFORM}$\r$\n\
               X-Plex-Platform-Version: $3"
	# Stack: $3 $2 $1 $0, reg: <headers> <token> <device> <version> (<accept> no longer needed on registers).
	StrCmp $1 "" +2                       # Skip one line if Auth Token value is empty.
	StrCpy $0 "$0$\r$\nX-Plex-Token: $1"  # Else append token header (updates <headers> in registers).

	# Restore registers from stack.
	Pop $3
	# Stack: $2 $1 $0, reg: <headers> <token> <device> $3
	Pop $2
	# Stack: $1 $0, reg: <headers> <token> $2 $3
	Pop $1
	# Stack: $0, reg: <headers> $1 $2 $3
	Exch $0
	# Stack: <headers>, reg: $0 $1 $2 $3
!macroend


# Get this computer name to use as device name.
!macro PlexDeviceName
	Push $0
	# Get this computer name.
	System::Call "kernel32.dll::GetComputerName(t .r0, *i ${NSIS_MAX_STRLEN}) i."
	Exch $0
!macroend


# Get formatted string containing Windows version.
!macro PlexPlatformVersion
	Push $0
	Push $1
	Push $2
	Push $3
	# Get Windows version.
	${WinVerGetMajor} $1
	${WinVerGetMinor} $2
	${WinVerGetBuild} $3
	StrCpy $0 "$1.$2 (Build $3)"
	Pop $3
	Pop $2
	Pop $1
	Exch $0
!macroend


# Function StrRep_
# Adapted from StrFunc.nsh
# 2004 Diego Pedroso - Based on functions by Hendri Adriaens.
!macro StrRep_
	# After this point:
	# $R0 = ReplacementString (input)
	# $R1 = StrToSearch (input)
	# $R2 = String (input)
	# $R3 = RepStrLen (temp)
	# $R4 = StrToSearchLen (temp)
	# $R5 = StrLen (temp)
	# $R6 = StartCharPos (temp)
	# $R7 = TempStrL (temp)
	# $R8 = TempStrR (temp)

	# Get input from user
	Exch $R0
	Exch
	Exch $R1
	Exch
	Exch 2
	Exch $R2
	Push $R3
	Push $R4
	Push $R5
	Push $R6
	Push $R7
	Push $R8

	# Return "String" if "StrToSearch" is ""
	${IfThen} $R1 == "" ${|} Goto Done ${|}

	# Get "ReplacementString", "String" and "StrToSearch" length
	StrLen $R3 $R0
	StrLen $R4 $R1
	StrLen $R5 $R2
	# Start "StartCharPos" counter
	StrCpy $R6 0

	# Loop until "StrToSearch" is found or "String" reaches its end
	${Do}
		# Remove everything before and after the searched part ("TempStrL")
		StrCpy $R7 $R2 $R4 $R6

		# Compare "TempStrL" with "StrToSearch"
		${If} $R7 == $R1
			# Split "String" to replace the string wanted
			StrCpy $R7 $R2 $R6 ;TempStrL

			# Calc: "StartCharPos" + "StrToSearchLen" = EndCharPos
			IntOp $R8 $R6 + $R4

			StrCpy $R8 $R2 "" $R8 ;TempStrR

			# Insert the new string between the two separated parts of "String"
			StrCpy $R2 $R7$R0$R8
			# Now calculate the new "StrLen" and "StartCharPos"
			StrLen $R5 $R2
			IntOp $R6 $R6 + $R3
			${Continue}
		${EndIf}

		# If not "StrToSearch", this could be "String" end
		${IfThen} $R6 >= $R5 ${|} ${ExitDo} ${|}
		# If not, continue the loop
		IntOp $R6 $R6 + 1
	${Loop}

	Done:

	# After this point:
	# $R0 = OutVar (output)

	# Return output to user
	StrCpy $R0 $R2
	Pop $R8
	Pop $R7
	Pop $R6
	Pop $R5
	Pop $R4
	Pop $R3
	Pop $R2
	Pop $R1
	Exch $R0
!macroend


# Function StrNSISToIO
# Adapted from StrFunc.nsh
# 2004 "bluenet" - Based on functions by Amir Szekely, Joost Verburg, Dave Laundon and Diego Pedroso.
!macro StrNSISToIO_
	# After this point:
	# -----------------
	# $R0 = String (input/output)
	# $R1 = StartCharPos (temp)
	# $R2 = StrLen (temp)
	# $R3 = TempStr (temp)
	# $R4 = TempRepStr (temp)*/

	# Get input from user
	Exch $R0
	Push $R1
	Push $R2
	Push $R3
	Push $R4

	# Get "String" length
	StrLen $R2 $R0

	# Loop until "String" end is reached
	${For} $R1 0 $R2
		# Get the next "String" character
		StrCpy $R3 $R0 1 $R1

		# Detect if current character is:
		${If} $R3 == "$\r" # Back-slash
			StrCpy $R4 "\r"
		${ElseIf} $R3 == "$\n" # Carriage return
			StrCpy $R4 "\n"
		${ElseIf} $R3 == "$\t" # Line feed
			StrCpy $R4 "\t"
		${ElseIf} $R3 == "\" # Tab
			StrCpy $R4 "\\"
		${Else} # Anything else
			StrCpy $R4 ""
		${EndIf}

		# Detect if "TempRepStr" is not empty
		${If} $R4 != ""
			# Replace the old character with the new ones
			StrCpy $R3 $R0 $R1
			IntOp $R1 $R1 + 1
			StrCpy $R0 $R0 "" $R1
			StrCpy $R0 "$R3$R4$R0"
			IntOp $R2 $R2 + 1 # Increase "StrLen"
		${EndIf}
	${Next}

	# Return output to user
	Pop $R4
	Pop $R3
	Pop $R2
	Pop $R1
	Exch $R0
!macroend

!endif
