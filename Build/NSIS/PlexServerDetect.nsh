# Functions for detecting Plex Media Server installation on the computer in NSIS installer.

!ifndef PLEX_SERVER_DETECT_INCLUDED
!define PLEX_SERVER_DETECT_INCLUDED

# Plex Media Server ProductID
!define PMS_PRODUCT_ID "{D2C8A865-4227-46D0-AD2B-D2BDFE3CFF48}"

!define PMS_CONTINUE_ANYWAY "Do you wish to continue installing anyway?"
!define PMS_ANOTHER_USER    "Plex Media Server is installed for another user on this computer. \
                             It is recommended to switch to that user account and run setup from it. \
                             ${PMS_CONTINUE_ANYWAY}"
!define PMS_NOT_INSTALLED   "Plex Media Server is not found on this computer. Running this setup \
                             is useless without it. ${PMS_CONTINUE_ANYWAY}"
!define PMS_INFO_ERROR      "There was an error retrieving information about your Plex Media Server \
                             installation. This setup may complete with errors. ${PMS_CONTINUE_ANYWAY} \
                             Error code was"

!define INSTALLSTATE_INVALIDARG -2  # An invalid parameter was passed to the function.
!define INSTALLSTATE_UNKNOWN    -1  # The product is neither advertised or installed.
!define INSTALLSTATE_ADVERTISED  1  # The product is advertised but not installed.
!define INSTALLSTATE_ABSENT      2  # The product is installed for a different user.
!define INSTALLSTATE_DEFAULT     5  # The product is installed for the current user.

!define INSTALLPROPERTY_PRODUCTICON "ProductIcon"

!define ERROR_SUCCESS   0  # The operation completed successfully.

!include PlexRequestsHelper.nsh

Var PMSIsInstalled

# Macro to generate function for detecting Plex Media Server install.
# Installer usage:
#     Function .onInit
#         Call CheckPlexMediaServerInstalled
#     FunctionEnd
# Uninstaller usage:
#     Function un.onInit
#         Call un.CheckPlexMediaServerInstalled
#     FunctionEnd
!macro CheckPlexMediaServerInstalled un
# This function checks whether the PMS is installed and shows a Message Box if not.
Function ${un}CheckPlexMediaServerInstalled
	Push $0
	System::Call "MSI::MsiQueryProductState(t '${PMS_PRODUCT_ID}') i .r0"
	StrCpy $PMSIsInstalled $0

	StrCmp $0 ${INSTALLSTATE_DEFAULT} Done
	StrCmp $0 ${INSTALLSTATE_ABSENT} AnotherUser
	Goto Default

	Default:
		MessageBox MB_ICONEXCLAMATION|MB_YESNO|MB_DEFBUTTON2 "${PMS_NOT_INSTALLED}" IDYES Done IDNO Cancel
	AnotherUser:
		MessageBox MB_ICONEXCLAMATION|MB_YESNO|MB_DEFBUTTON2 "${PMS_ANOTHER_USER}" IDYES Done IDNO Cancel
	Cancel:
		Abort
	Done:
		Pop $0
FunctionEnd
!macroend

!insertmacro CheckPlexMediaServerInstalled ""
!insertmacro CheckPlexMediaServerInstalled "un."


# Macro to generate function for checking if Plex Media Server is installed.
# Installer usage:
#     Call IsPlexMediaServerInstalled
# Uninstaller usage:
#     Call un.IsPlexMediaServerInstalled
# General usage (checking result, right after function call):
#     Pop $0
#     StrCmp $0 "${INSTALLSTATE_DEFAULT}" 0 +2  ; Skip one line if PMS not installed.
#     Call un.PlexRefreshChannels               ; Refresh Channels list.
!macro IsPlexMediaServerInstalled un
# This function checks whether the PMS is installed and shows a Message Box if not.
Function ${un}IsPlexMediaServerInstalled
	StrCmp $PMSIsInstalled "" 0 +2
	Call ${un}CheckPlexMediaServerInstalled
	Push $PMSIsInstalled
FunctionEnd
!macroend

!insertmacro IsPlexMediaServerInstalled ""
!insertmacro IsPlexMediaServerInstalled "un."


# This function retrieves the PMS icon from Windows Installer registry.
# Adapted from http://nsis.sourceforge.net/MSI_Functions
# Original author: Zinthose <http://nsis.sourceforge.net/User:Zinthose>
Function GetPlexMediaServerIcon
	Push $0  # Will be used for Return Codes.
	Push $1  # Will be used for Pointer To Buffer Size.
	Push $2  # Will be used for Pointer to Buffer.
	Push $3  # Will be used for Buffer Size.

	# Create/Allocate a Pointer to a DWORD for the Buffer Size.
	System::Alloc 2
	Pop $1

	# Determine out big the buffer need to be.
	System::Call "MSI::MsiGetProductInfo(t '${PMS_PRODUCT_ID}', t '${INSTALLPROPERTY_PRODUCTICON}', n, i $1) i .r0"
	StrCmp $0 ${ERROR_SUCCESS} +2
	MessageBox MB_ICONEXCLAMATION|MB_YESNO|MB_DEFBUTTON2 "1${PMS_INFO_ERROR} $0." IDNO Cancel

	# Get the requested buffer size.
	System::Call "*$1(&i2 .r3)"

	# Increase bufer size to account for required Null Terminator.
	IntOp $3 $3 + 1
	System::Call "*$1(&i2 r3)"

	# Allocate Buffer.
	System::Alloc $3
	Pop $2

	# Get the Parameter Value.
	System::Call "MSI::MsiGetProductInfo(t '${PMS_PRODUCT_ID}', t '${INSTALLPROPERTY_PRODUCTICON}', i $2, i $1) i .r0"
	StrCmp $0 ${ERROR_SUCCESS} +2
	MessageBox MB_ICONEXCLAMATION|MB_YESNO|MB_DEFBUTTON2 "${PMS_INFO_ERROR} $0." IDNO Cancel

	# Read Property.
	System::Call "*$2(&t$3 .r0)"

	# Free Memory.
	System::Free $1
	System::Free $2

	Goto Done

	Cancel:
		Abort
	Done:
		Pop $3
		Pop $2
		Pop $1
		Exch $0
FunctionEnd

!endif
