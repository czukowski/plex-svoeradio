# Functions for running requests to Plex Media Server installed on the computer in NSIS installer.
# Requires NSIS Inetc plugin: <http://nsis.sourceforge.net/Inetc_plug-in>

!ifndef PLEX_SERVER_MANAGE_INCLUDED
!define PLEX_SERVER_MANAGE_INCLUDED

!define PLEX_SYSTEM_RESTART     "http://127.0.0.1:32400/:/plugins/com.plexapp.system/restart"
!define PLEX_PLUGIN_RESTART     "http://127.0.0.1:32400/:/plugins/%IDENT%/restart"
!define PLEX_PLUGIN_PLACEHOLDER "%IDENT%"

!define PLEX_MANAGE_RESTART_ERROR "The installer was unable to restart your Plex Media Server, you may need to do so manually. \
                                   Sorry for wasting your time with logging in and all."

!define PLEX_DETAIL_PLUGIN_RESTARTED  "Plex.Tv: Plex Plug-In restarted"
!define PLEX_DETAIL_SERVER_RESTARTED  "Plex.Tv: Plex Server restarted"
!define PLEX_DETAIL_PLUGIN_RESTARTING "Plex.Tv: Restarting Plex Server..."
!define PLEX_DETAIL_RESTART_FAILED    "Plex.Tv: Plex Server restart failed, you may need to do so manually"

!include PlexRequestsHelper.nsh
!include PlexTvLogin.nsh

# Macro to generate function for restarting Plex Media Server if installer is logged in.
# Installer usage:
#     Call PlexRefreshChannelsIfLoggedIn
# Uninstaller usage:
#     Call un.PlexRefreshChannelsIfLoggedIn
!macro PlexRefreshChannelsIfLoggedIn un
Function ${un}PlexRefreshChannelsIfLoggedIn
	# Stack: -, reg: $0 $1 $2 $3
	Push $0
	# Stack: $0, reg: $0 $1 $2 $3
	Call ${un}GetPlexTvAuthToken
	# Stack: <token> $0, reg: $0 $1 $2 $3
	Pop $0
	# Stack: $0, reg: <token> $1 $2 $3
	StrCmp $0 "" Done   # If there's no token, we're done.
	Push $0
	# Stack: <token> $0, reg: <token> $1 $2 $3
	Call ${un}PlexRefreshChannels
	# Stack: $0, reg: <token> $1 $2 $3
	Done:
	Pop $0
	# Stack: -, $0 $1 $2 $3
FunctionEnd
!macroend

!insertmacro PlexRefreshChannelsIfLoggedIn ""
!insertmacro PlexRefreshChannelsIfLoggedIn "un."


# Macro to generate function for restarting Plex Media Server.
# Installer usage:
#     Call GetPlexTvAuthToken  ; Stack now has Auth Token on top.
#     Call PlexRefreshChannels
# Uninstaller usage:
#     Call un.GetPlexTvAuthToken
#     Call un.PlexRefreshChannels
!macro PlexRefreshChannels un
Function ${un}PlexRefreshChannels
	# Stack listings below ordered from top. Registers ($0, $1,..) represent original values, ie before function call
	# Stack: <token>, reg: $0 $1 $2 $3
	Push "application/xml"
	# Stack: <accept> <token>, reg: $0 $1 $2 $3
	${CallArtificialFunction} PlexTvRequestHeaders
	# Stack: <headers>, reg: $0 $1 $2 $3
	Exch $0
	# Stack: $0, reg: <headers> $1 $2 $3
	Push $1
	# Stack: $1 $0, reg: <headers> $1 $2 $3
	Push $2
	# Stack: $2 $1 $0, reg: <headers> $1 $2 $3
	Push $3
	# Stack: $3 $2 $1 $0, reg: <headers> $1 $2 $3

	# TODO: detect previously installed version.
	StrCmp "" "" RestartServer       # No previous version detected, restart server
	StrCmp ${un} "" 0 RestartServer  # Also restart server on uninstall

	RestartPlugin:
		Push "${PLEX_PLUGIN_RESTART}"
		# Stack: <string> $3 $2 $1 $0, reg: <headers> $1 $2 $3
		Push "${PLEX_PLUGIN_PLACEHOLDER}"
		# Stack: <search> <string> $3 $2 $1 $0, reg: <headers> $1 $2 $3
		Push "${PLUGIN_IDENTIFIER}"
		# Stack: <replace> <search> <string> $3 $2 $1 $0, reg: <headers> $1 $2 $3
		${CallArtificialFunction} StrRep_
		# Stack: <url> $3 $2 $1 $0, reg: <headers> $1 $2 $3
		Pop $1
		# Stack: $3 $2 $1 $0, reg: <headers> <url> $2 $3
		# Restart just the plugin
		ClearErrors
		Inetc::get /SILENT /TOSTACK /HEADER $0 $1 "" /END
		Pop $2
		Pop $3
		# Stack: $3 $2 $1 $0, reg: <headers> <url> <status> <response>
		StrCmp $2 "OK" 0 RestartServer  # Try restarting server on error
		DetailPrint "${PLEX_DETAIL_PLUGIN_RESTARTED}"
		Goto DoneAll

	RestartServer:
		# Restart server
		DetailPrint "${PLEX_DETAIL_PLUGIN_RESTARTING}"
		ClearErrors
		Inetc::get /SILENT /TOSTACK /HEADER $0 ${PLEX_SYSTEM_RESTART} "" /END
		Pop $2
		Pop $3
		# Stack: $3 $2 $1 $0, reg: <headers> <url> <status> <response>
		StrCmp $2 "OK" 0 RestartServerError
		DetailPrint "${PLEX_DETAIL_SERVER_RESTARTED}"
		Goto DoneAll

	RestartServerError:
		DetailPrint "${PLEX_DETAIL_RESTART_FAILED}"
		MessageBox MB_OK "${PLEX_MANAGE_RESTART_ERROR} $2 $3" IDOK DoneAll

	DoneAll:
		ClearErrors
		# Stack: $3 $2 $1 $0, reg: <headers> <url> <status> <response>
		Pop $3
		# Stack: $2 $1 $0, reg: <headers> <url> <status> $3
		Pop $2
		# Stack: $1 $0, reg: <headers> <url> $2 $3
		Pop $1
		# Stack: $0, reg: <headers> $1 $2 $3
		Pop $0
		# Stack: -, $0 $1 $2 $3
FunctionEnd
!macroend

!insertmacro PlexRefreshChannels ""
!insertmacro PlexRefreshChannels "un."

!endif
