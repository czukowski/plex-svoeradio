# Functions for getting Plex Media Server auth token in NSIS installer.
# Requires NSIS NsJSON plugin: <http://nsis.sourceforge.net/NsJSON_plug-in>

!ifndef PLEX_TV_LOGIN_INCLUDED
!define PLEX_TV_LOGIN_INCLUDED

!include LogicLib.nsh
!include nsDialogs.nsh
!include PlexServerDetect.nsh
!include PlexRequestsHelper.nsh
!include WinMessages.nsh
!include Util.nsh

!define PLEX_PIN_PAGE    "https://plex.tv/pin"
!define PLEX_PINS_URL    "https://plex.tv/pins.json"
!define PLEX_VERIFY_URL  "https://plex.tv/pins/$PlexLoginPinID.json"
!define PLEX_ACCOUNT_URL "https://plex.tv/users/account.json"

!define PLEX_LOGIN_BUTTON_TEXT        "Log In"
!define PLEX_LOGIN_COPY_TEXT          "Copy to clipboard"
!define PLEX_LOGIN_ENTER_PIN_TEXT     "Now open your browser and load Plex.Tv/pin page to enter the PIN displayed \
                                       below and then click 'Verify'."
!define PLEX_LOGIN_INSTRUCTIONS_TEXT  "Clicking 'Log In' button will open your web browser at the Plex.Tv server \
                                       where you'll need to log in and enter the PIN code that you'll see below."
!define PLEX_LOGIN_NO_AUTH_TOKEN_TEXT "Plex.Tv server did not return an authorization token. Please check you have \
                                       logged in and then click 'Verify' again."
!define PLEX_LOGIN_OPEN_BROWSER_TEXT  "Open Browser"
!define PLEX_LOGIN_REQUEST_PIN_TEXT   "Requesting PIN"
!define PLEX_LOGIN_VERIFY_TEXT        "Verify"
!define PLEX_LOGIN_REQUEST_FAILED     "An issue has occurred while contacting Plex.Tv server. You may either repeat \
                                       the action or skip this step and restart your Plex Media Server manually after \
                                       the setup completes. The error code was"
!define PLEX_LOGIN_SUCCESSFUL_TEXT    "You're all set! ${PLEX_PRODUCT_NAME} is now linked with your account. Please \
                                       continue to the next step by clicking 'Next' button."
!define PLEX_LOGIN_USER_TITLE_TEXT    "Logged in user: $PlexLoginUserTitleValue"
!define PLEX_LOGIN_WELCOME_TEXT       "Here you can log in to your Plex Media Server to allow the installer to \
                                       refresh Plex channels list automatically. No personal information will be transmitted. \
                                       You may skip this step by clicking 'Next' button, but you'll have to restart yout \
                                       Plex Server manually after the setup completes."

!define PLEX_DETAIL_LOGGED_IN  "Plex.Tv: Logged in as $PlexLoginUserTitleValue"

Var PlexTvLoginDialog
Var PlexLoginWelcomeLabel
Var PlexLoginInstructionsLabel
Var PlexLoginPINButton
Var PlexLoginEnterPINLabel
Var PlexLoginPINLabel
Var PlexLoginCopyButton
Var PlexLoginOpenBrowserButton
Var PlexLoginVerifyButton
Var PlexLoginAuthToken
Var PlexLoginPinID
Var PlexLoginPinValue
Var PlexLoginSuccessLabel
Var PlexLoginUserTitleLabel
Var PlexLoginUserTitleValue


# Macro to print logged in Plex.Tv username. Can be used both in `install` and `uninstall` sections
# Usage: `!insertmacro DetailPrintPlexTvLoggedInUser`
!macro DetailPrintPlexTvLoggedInUser
	${If} $PlexLoginAuthToken != ""
	${AndIf} $PlexLoginUserTitleValue != ""
		DetailPrint "${PLEX_DETAIL_LOGGED_IN}"
	${EndIf}
!macroend


# Macro to generate function for retrieving Plex authorization token, if user has logged in.
# Else the return value will be empty.
# Installer usage:
#     Call GetPlexTvAuthToken
# Uninstaller usage:
#     Call un.GetPlexTvAuthToken
# General usage (checking result, right after function call):
#     ; Stack now has Auth Token on top.
#     Push "${PLUGIN_IDENTIFIER}" ; Push Plex plugin identifier to the stack as the 1st argument. Auth Token will become 2nd.
#     Call PlexRefreshChannels    ; Refresh Channels list.
!macro GetPlexTvAuthToken un
Function ${un}GetPlexTvAuthToken
	Push $PlexLoginAuthToken
FunctionEnd
!macroend

!insertmacro GetPlexTvAuthToken ""
!insertmacro GetPlexTvAuthToken "un."


# Macro to generate function for Plex.Tv login custom page.
# Installer usage: `Page custom PlexTvLoginPageEnter "" ": ${PLEX_LOGIN_BUTTON_TEXT}"`
# Uninstaller usage: `UninstPage custom un.PlexTvLoginPageEnter "" ": ${PLEX_LOGIN_BUTTON_TEXT}"`
!macro PlexTvLoginPageEnter un
# On page enter function for ${un}installer.
Function ${un}PlexTvLoginPageEnter
	# Check if PMS is installed and skip this step if not.
	Call ${un}IsPlexMediaServerInstalled
	Pop $0
	StrCmp $0 "${INSTALLSTATE_DEFAULT}" +2
	Abort

	# Create login dialog and controls.
	nsDialogs::Create 1018
	Pop $PlexTvLoginDialog
	${If} $PlexTvLoginDialog == error
		Abort
	${EndIf}

	${NSD_CreateLabel} 5% 10u 90% 32u "${PLEX_LOGIN_WELCOME_TEXT}"
	Pop $PlexLoginWelcomeLabel
	${NSD_CreateLabel} 5% 46u 90% 16u "${PLEX_LOGIN_INSTRUCTIONS_TEXT}"
	Pop $PlexLoginInstructionsLabel

	${NSD_CreateButton} 40% 74u 20% 16u "${PLEX_LOGIN_BUTTON_TEXT}..."
	Pop $PlexLoginPINButton
	${NSD_AddStyle} $PlexLoginPINButton ${SS_CENTER}
	${NSD_OnClick} $PlexLoginPINButton ${un}PlexLoginButtonOnClick_
	SendMessage $PlexLoginPINButton ${BM_SETSTYLE} ${BS_PUSHBUTTON} 1  # This is to force remove default border.

	${NSD_CreateLabel} 5% 46u 90% 16u "${PLEX_LOGIN_ENTER_PIN_TEXT}"
	Pop $PlexLoginEnterPINLabel
	ShowWindow $PlexLoginEnterPINLabel ${SW_HIDE}

	${NSD_CreateLabel} 40% 68u 20% 20u "****"
	Pop $PlexLoginPINLabel
	ShowWindow $PlexLoginPINLabel ${SW_HIDE}
	CreateFont $0 "Arial" 18
	SendMessage $PlexLoginPINLabel ${WM_SETFONT} $0 1
	${NSD_AddStyle} $PlexLoginPINLabel ${SS_CENTER}

	${NSD_CreateButton} 13% 90u 24% 14u "${PLEX_LOGIN_COPY_TEXT}"
	Pop $PlexLoginCopyButton
	ShowWindow $PlexLoginCopyButton ${SW_HIDE}
	${NSD_AddStyle} $PlexLoginCopyButton ${SS_CENTER}
	${NSD_OnClick} $PlexLoginCopyButton ${un}PlexLoginCopyButtonOnClick_

	${NSD_CreateButton} 38% 90u 24% 14u "${PLEX_LOGIN_OPEN_BROWSER_TEXT}"
	Pop $PlexLoginOpenBrowserButton
	ShowWindow $PlexLoginOpenBrowserButton ${SW_HIDE}
	${NSD_AddStyle} $PlexLoginOpenBrowserButton ${SS_CENTER}
	${NSD_OnClick} $PlexLoginOpenBrowserButton ${un}PlexLoginOpenBrowserButtonOnClick_

	${NSD_CreateButton} 63% 90u 24% 14u "${PLEX_LOGIN_VERIFY_TEXT}"
	Pop $PlexLoginVerifyButton
	ShowWindow $PlexLoginVerifyButton ${SW_HIDE}
	${NSD_AddStyle} $PlexLoginVerifyButton ${SS_CENTER}
	${NSD_OnClick} $PlexLoginVerifyButton ${un}PlexLoginVerifyButtonOnClick_

	${NSD_CreateLabel} 5% 46u 90% 16u "${PLEX_LOGIN_SUCCESSFUL_TEXT}"
	Pop $PlexLoginSuccessLabel
	ShowWindow $PlexLoginSuccessLabel ${SW_HIDE}

	${NSD_CreateLabel} 5% 66u 90% 16u ""
	Pop $PlexLoginUserTitleLabel
	ShowWindow $PlexLoginUserTitleLabel ${SW_HIDE}
	${NSD_AddStyle} $PlexLoginUserTitleLabel ${SS_CENTER}

	${If} $PlexLoginPinID != ""
	${AndIf} $PlexLoginPinValue != ""
		Call ${un}PlexLoginShowVerifyStep_
	${EndIf}
	${If} $PlexLoginAuthToken != ""
	${AndIf} $PlexLoginUserTitleValue != ""
		Call ${un}PlexLoginShowSuccessStep_
	${EndIf}

	nsDialogs::Show
	ClearErrors
FunctionEnd
!macroend
 
!insertmacro PlexTvLoginPageEnter ""
!insertmacro PlexTvLoginPageEnter "un."


# Helper function to compose NsJSON config for requests to Plex.Tv.
# Three function arguments.
!macro PlexTvRequestJSONConfig_
	# Stack: <verb> <url> <params>, reg: $0 $1 $2 $3
	Exch $0
	# Stack: $0 <url> <params>, reg: <verb> $1 $2 $3
	Exch
	# Stack: <url> $0 <params>, reg: <verb> $1 $2 $3
	Exch $1
	# Stack: $1 $0 <params>, reg: <verb> <url> $2 $3
	Exch 2
	# Stack: <params> $0 $1, reg: <verb> <url> $2 $3
	Exch $2
	# Stack: $2 $0 $1, reg: <verb> <url> <params> $3
	Push $3    # Push register we'll be using to stack.
	# Stack: $3 $2 $0 $1, reg: <verb> <url> <params> $3

	# Call PlexTvRequestHeaders function.
	Push ""                                 # Do not use auth token here.
	# Stack: <""> $3 $2 $0 $1, reg: <verb> <url> <params> $3
	Push "application/json"                 # Accept type.
	# Stack: <accept> <""> $4 $3 $2 $0 $1, reg: <verb> <url> <params> $3
	${CallArtificialFunction} PlexTvRequestHeaders
	# Stack: <headers> $3 $2 $0 $1, reg: <verb> <url> <params> $3
	# Note: new lines in headers must be left alone!
	# It will be invalid JSON, but HTTP request via NsJSON plugin don't work with newlines converted to `\r\n`.
	# ${CallArtificialFunction} StrNSISToIO_  # Replace new lines with \r\n for valid JSON string.
	Pop $3                                  # Pop function return value from stack.
	# Stack: $3 $2 $0 $1, reg: <verb> <url> <params> <headers>
	# Build JSON string.
	StrCpy $0  `{ "Verb": "$0", "Url": "$1", "Params": "$2", "ParamsType": "raw", "Headers": "$3" }`
	# Stack: $3 $2 $0 $1, reg: <json> <url> <params> <headers>
	Pop $3     # Restore registers before returning from the function.
	# Stack: $2 $0 $1, reg: <json> <url> <params> $3
	Pop $2
	# Stack: $0 $1, reg: <json> <url> $2 $3
	Exch
	# Stack: $1 $0, reg: <json> <url> $2 $3
	Pop $1
	# Stack: $0, reg: <json> $1 $2 $3
	Exch $0
	# Stack: <json>, reg: $0 $1 $2 $3
!macroend


# Helper function to copy string to clipboard.
# Adapted from <http://nsis.sourceforge.net/Copy_to,_or_get_from_Windows_Clipboard>
# Original authors: Kichik <http://nsis.sourceforge.net/User:Kichik> and Brainsucker <http://nsis.sourceforge.net/User:Brainsucker>
!macro CopyToClipboard_
	Exch $0  # Input string
	Push $1
	Push $2
	System::Call "user32::OpenClipboard(i 0)"
	System::Call "user32::EmptyClipboard()"
	StrLen $1 $0
	IntOp $1 $1 + 1
	System::Call "kernel32::GlobalAlloc(i 2, i r1) i.r1"
	System::Call "kernel32::GlobalLock(i r1) i.r2"
	System::Call "kernel32::lstrcpyA(i r2, t r0)"
	System::Call "kernel32::GlobalUnlock(i r1)"
	System::Call "user32::SetClipboardData(i 1, i r1)"
	System::Call "user32::CloseClipboard()"
	Pop $2
	Pop $1
	Pop $0
!macroend


!macro PlexLoginShowVerifyStep_ un
# Switches controls to display Verify step.
Function ${un}PlexLoginShowVerifyStep_
	${NSD_SetText} $PlexLoginPINLabel "$PlexLoginPinValue"
	ShowWindow $PlexLoginPINButton ${SW_HIDE}
	ShowWindow $PlexLoginInstructionsLabel ${SW_HIDE}
	ShowWindow $PlexLoginEnterPINLabel ${SW_SHOW}
	ShowWindow $PlexLoginPINLabel ${SW_SHOW}
	ShowWindow $PlexLoginCopyButton ${SW_SHOW}
	ShowWindow $PlexLoginOpenBrowserButton ${SW_SHOW}
	ShowWindow $PlexLoginVerifyButton ${SW_SHOW}
	SendMessage $PlexLoginCopyButton ${BM_SETSTYLE} ${BS_PUSHBUTTON} 1
	SendMessage $PlexLoginOpenBrowserButton ${BM_SETSTYLE} ${BS_PUSHBUTTON} 1
	SendMessage $PlexLoginVerifyButton ${BM_SETSTYLE} ${BS_PUSHBUTTON} 1
FunctionEnd
!macroend
 
!insertmacro PlexLoginShowVerifyStep_ ""
!insertmacro PlexLoginShowVerifyStep_ "un."


!macro PlexLoginShowSuccessStep_ un
# Switches controls to display Success step.
Function ${un}PlexLoginShowSuccessStep_
	${NSD_SetText} $PlexLoginUserTitleLabel "${PLEX_LOGIN_USER_TITLE_TEXT}"
	ShowWindow $PlexLoginEnterPINLabel ${SW_HIDE}
	ShowWindow $PlexLoginPINLabel ${SW_HIDE}
	ShowWindow $PlexLoginCopyButton ${SW_HIDE}
	ShowWindow $PlexLoginOpenBrowserButton ${SW_HIDE}
	ShowWindow $PlexLoginVerifyButton ${SW_HIDE}
	ShowWindow $PlexLoginSuccessLabel ${SW_SHOW}
	ShowWindow $PlexLoginUserTitleLabel ${SW_SHOW}
FunctionEnd
!macroend
 
!insertmacro PlexLoginShowSuccessStep_ ""
!insertmacro PlexLoginShowSuccessStep_ "un."


!macro PlexLoginButtonOnClick_ un
# On Log In button click function.
Function ${un}PlexLoginButtonOnClick_
	# Clear old values.
	StrCpy $PlexLoginPinValue ""
	StrCpy $PlexLoginPinID ""

	# Create HTTP request.
	Push ""
	Push "${PLEX_PINS_URL}"
	Push "POST"
	${CallArtificialFunction} PlexTvRequestJSONConfig_
	Pop $0

	nsJSON::Set /tree HttpWebRequest /value "$0"
	nsJSON::Set /tree HttpGetPinResponse /http HttpWebRequest
	${If} ${Errors}
		MessageBox MB_OK "${PLEX_LOGIN_REQUEST_FAILED} -1001 (HTTP request error)" IDOK Done
	${EndIf}

	# Check response status code, should be 201.
	nsJSON::Get /tree HttpGetPinResponse "StatusCode" /end
	Pop $0
	StrCmp $0 "201" +2
	MessageBox MB_OK "${PLEX_LOGIN_REQUEST_FAILED} $0 (Unexpected HTTP status code)" IDOK Done

	# Get response body.
	nsJSON::Get /tree HttpGetPinResponse "Output" /end
	Pop $0  # Return text.
	nsJSON::Set /tree GetPinResponse /value `$0`
	${If} ${Errors}
		MessageBox MB_OK "${PLEX_LOGIN_REQUEST_FAILED} -1002 (HTTP response decode error)" IDOK Done
	${EndIf}

	nsJSON::Get /tree GetPinResponse "pin" "code" /end
	Pop $PlexLoginPinValue  # PIN code.

	nsJSON::Get /tree GetPinResponse "pin" "id" /end
	Pop $PlexLoginPinID     # Request ID.

	Call ${un}PlexLoginShowVerifyStep_

	Done:
		ClearErrors
FunctionEnd
!macroend
 
!insertmacro PlexLoginButtonOnClick_ ""
!insertmacro PlexLoginButtonOnClick_ "un."


!macro PlexLoginCopyButtonOnClick_ un
# Copies PIN code to clipboard.
Function ${un}PlexLoginCopyButtonOnClick_
	Push $PlexLoginPinValue
	${CallArtificialFunction} CopyToClipboard_
FunctionEnd
!macroend
 
!insertmacro PlexLoginCopyButtonOnClick_ ""
!insertmacro PlexLoginCopyButtonOnClick_ "un."


!macro PlexLoginOpenBrowserButtonOnClick_ un
# Opens browser at Plex.Tv/PIN page.
Function ${un}PlexLoginOpenBrowserButtonOnClick_
	ExecShell open "${PLEX_PIN_PAGE}"
FunctionEnd
!macroend
 
!insertmacro PlexLoginOpenBrowserButtonOnClick_ ""
!insertmacro PlexLoginOpenBrowserButtonOnClick_ "un."


!macro PlexLoginVerifyButtonOnClick_ un
# Verifies that user has authorized the PIN code.
Function ${un}PlexLoginVerifyButtonOnClick_
	# Check required variables are set.
	${If} $PlexLoginPinID == ""
	${OrIf} $PlexLoginPinValue == ""
		Goto Done
	${EndIf}

	# Clear old values.
	StrCpy $PlexLoginAuthToken ""
	StrCpy $PlexLoginUserTitleValue ""

	# Create HTTP request.
	Push ""
	Push "${PLEX_VERIFY_URL}"
	Push "GET"
	${CallArtificialFunction} PlexTvRequestJSONConfig_
	Pop $0

	nsJSON::Set /tree HttpWebRequest /value $0
	nsJSON::Set /tree HttpVerifyResponse /http HttpWebRequest
	${If} ${Errors}
		MessageBox MB_OK "${PLEX_LOGIN_REQUEST_FAILED} -2001 (HTTP request error)" IDOK Done
	${EndIf}

	# Check response status code, should be 201.
	nsJSON::Get /tree HttpVerifyResponse "StatusCode" /end
	Pop $0
	StrCmp $0 "200" +2
	MessageBox MB_OK "${PLEX_LOGIN_REQUEST_FAILED} $0 (Unexpected HTTP status code)" IDOK Done

	# Get response body.
	nsJSON::Get /tree HttpVerifyResponse "Output" /end
	Pop $0  # Return text.
	nsJSON::Set /tree VerifyResponse /value `$0`
	${If} ${Errors}
		MessageBox MB_OK "${PLEX_LOGIN_REQUEST_FAILED} -2002 (HTTP response decode error)" IDOK Done
	${EndIf}

	nsJSON::Get /tree VerifyResponse "pin" "auth_token" /end
	Pop $0  # Temp token.
	StrCmp $0 "null" 0 +2
	MessageBox MB_ICONEXCLAMATION|MB_OK "${PLEX_LOGIN_NO_AUTH_TOKEN_TEXT}" IDOK Done
	StrCpy $PlexLoginAuthToken $0

	# Get user info.
	Push "X-Plex-Token=$0"
	Push "${PLEX_ACCOUNT_URL}"
	Push "GET"
	${CallArtificialFunction} PlexTvRequestJSONConfig_
	Pop $0


	nsJSON::Set /tree HttpWebRequest /value $0
	nsJSON::Set /tree HttpAccountResponse /http HttpWebRequest
	${If} ${Errors}
		MessageBox MB_OK "${PLEX_LOGIN_REQUEST_FAILED} -3001 (HTTP request error)" IDOK Done
	${EndIf}

	# Check response status code, should be 200.
	nsJSON::Get /tree HttpAccountResponse "StatusCode" /end
	Pop $0
	StrCmp $0 "200" +2
	MessageBox MB_OK "${PLEX_LOGIN_REQUEST_FAILED} $0 (Unexpected HTTP status code)" IDOK Done

	# Get response body.
	nsJSON::Get /tree HttpAccountResponse "Output" /end
	Pop $0  # Return text.
	nsJSON::Set /tree AccountResponse /value `$0`
	${If} ${Errors}
		MessageBox MB_OK "${PLEX_LOGIN_REQUEST_FAILED} -3002 (HTTP response decode error)" IDOK Done
	${EndIf}

	nsJSON::Get /tree AccountResponse "user" "title" /end
	Pop $PlexLoginUserTitleValue  # User title.

	Call ${un}PlexLoginShowSuccessStep_

	Done:
		ClearErrors
FunctionEnd
!macroend
 
!insertmacro PlexLoginVerifyButtonOnClick_ ""
!insertmacro PlexLoginVerifyButtonOnClick_ "un."

!endif
