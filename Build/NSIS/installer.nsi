Unicode true

!define APPNAME "SvoeRadio.fm Channel"
!define COMPANYNAME "Korney Czukowski"
!define DESCRIPTION "Support for SvoeRadio.fm for Plex Media Server"

!include version.nsh
!define VERSIONMAJOR 1  # These three must be integers
!define VERSIONMINOR 1
!define VERSIONBUILD ${BUILD_NUMBER}

!define HELPURL "https://bitbucket.org/czukowski/plex-svoeradio"                 # "Support Information" link
!define UPDATEURL "https://bitbucket.org/czukowski/plex-svoeradio/downloads"     # "Product Updates" link
!define ABOUTURL "https://forums.plex.tv/discussion/233957/release-svoyo-radio"  # "Publisher" link

!define ARP "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}"

!define PLUGIN_IDENTIFIER "com.plexapp.plugins.svoeradiofm"

InstallDir "$LOCALAPPDATA\Plex Media Server\Plug-Ins\SvoeRadioFm.bundle"

Name "${APPNAME}"                  # This will be in the installer/uninstaller's title bar.
OutFile "svoeradio-fm-bundle.exe"  # Installer EXE filename.
LicenseData "license.txt"          # RTF or plain text file - if it is text, it must be in the DOS text format (\r\n).

!include PlexServerDetect.nsh
!include PlexServerManage.nsh
!include PlexTvLogin.nsh

# Initialization functions.
Function .onInit
	Call CheckPlexMediaServerInstalled
FunctionEnd

Function un.onInit
	Call un.CheckPlexMediaServerInstalled
FunctionEnd

# Installer pages list.
Page license
Page custom PlexTvLoginPageEnter "" ": ${PLEX_LOGIN_BUTTON_TEXT}"
Page directory
Page instfiles

# Uninstaller pages list.
UninstPage uninstconfirm
UninstPage custom un.PlexTvLoginPageEnter "" ": ${PLEX_LOGIN_BUTTON_TEXT}"
UninstPage instfiles

Section "install"
	!insertmacro DetailPrintPlexTvLoggedInUser
	SetOutPath "$INSTDIR\Contents\Code"
	File "..\..\Contents\Code\__init__.py"
	SetOutPath "$INSTDIR\Contents\Code\locale"
	File "..\..\Contents\Code\locale\__init__.py"
	SetOutPath "$INSTDIR\Contents\Resources"
	File "..\..\Contents\Resources\logo.jpg"
	File "..\..\Contents\Resources\back_22.jpg"
	SetOutPath "$INSTDIR\Contents\Shared Code"
	File "..\..\Contents\Shared Code\svoeradio.pys"
	File "..\..\Contents\Shared Code\playrequest.pys"
	SetOutPath "$INSTDIR\Contents\Strings"
	File "..\..\Contents\Strings\ru.json"
	SetOutPath "$INSTDIR\Contents\URL Services\AudioArchive"
	File "..\..\Contents\URL Services\AudioArchive\ServiceCode.pys"
	SetOutPath "$INSTDIR\Contents\URL Services\Latest"
	File "..\..\Contents\URL Services\Latest\ServiceCode.pys"
	SetOutPath "$INSTDIR\Contents"
	File "..\..\Contents\Info.plist"
	File "..\..\Contents\DefaultPrefs.json"

	# Uninstaller - See function un.onInit and section "uninstall" for configuration
	WriteUninstaller "$INSTDIR\uninstall.exe"

	# Get Plex Media Server icon path.
	Var /GLOBAL PMSICON
	Call GetPlexMediaServerIcon
	Pop $0
	StrCpy $PMSICON "$0"

	# Registry information for add/remove programs
	WriteRegStr HKLM "${ARP}" "DisplayName" "Plex Media Server - ${APPNAME}"
	WriteRegStr HKLM "${ARP}" "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
	WriteRegStr HKLM "${ARP}" "QuietUninstallString" "$\"$INSTDIR\uninstall.exe$\" /S"
	WriteRegStr HKLM "${ARP}" "InstallLocation" "$INSTDIR"
	WriteRegStr HKLM "${ARP}" "DisplayIcon" "$\"$PMSICON$\""
	WriteRegStr HKLM "${ARP}" "Publisher" "${COMPANYNAME}"
	WriteRegStr HKLM "${ARP}" "HelpLink" "${HELPURL}"
	WriteRegStr HKLM "${ARP}" "URLUpdateInfo" "${UPDATEURL}"
	WriteRegStr HKLM "${ARP}" "URLInfoAbout" "${ABOUTURL}"
	WriteRegStr HKLM "${ARP}" "DisplayVersion" "${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONBUILD}"
	WriteRegDWORD HKLM "${ARP}" "VersionMajor" ${VERSIONMAJOR}
	WriteRegDWORD HKLM "${ARP}" "VersionMinor" ${VERSIONMINOR}
	# There is no option for modifying or repairing the install
	WriteRegDWORD HKLM "${ARP}" "NoModify" 1
	WriteRegDWORD HKLM "${ARP}" "NoRepair" 1
	# Computing EstimatedSize so Add/Remove Programs can accurately report the size
	!include "FileFunc.nsh"
	${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2
	IntFmt $0 "0x%08X" $0
	WriteRegDWORD HKLM "${ARP}" "EstimatedSize" "$0"

	# Restart Plex Media Server
	Call PlexRefreshChannelsIfLoggedIn
SectionEnd

Section "uninstall"
	!insertmacro DetailPrintPlexTvLoggedInUser
	# Remove files
	Delete "$INSTDIR\Contents\Code\locale\__init__.py"
	RmDir "$INSTDIR\Contents\Code\locale"
	Delete "$INSTDIR\Contents\Code\__init__.py"
	RmDir "$INSTDIR\Contents\Code"
	Delete "$INSTDIR\Contents\Resources\logo.jpg"
	Delete "$INSTDIR\Contents\Resources\back_22.jpg"
	RmDir "$INSTDIR\Contents\Resources"
	Delete "$INSTDIR\Contents\Shared Code\svoeradio.pys"
	Delete "$INSTDIR\Contents\Shared Code\playrequest.pys"
	RmDir "$INSTDIR\Contents\Shared Code"
	Delete "$INSTDIR\Contents\Strings\ru.json"
	RmDir "$INSTDIR\Contents\Strings"
	Delete "$INSTDIR\Contents\URL Services\AudioArchive\ServiceCode.pys"
	RmDir "$INSTDIR\Contents\URL Services\AudioArchive"
	Delete "$INSTDIR\Contents\URL Services\Latest\ServiceCode.pys"
	RmDir "$INSTDIR\Contents\URL Services\Latest"
	RmDir "$INSTDIR\Contents\URL Services"
	Delete "$INSTDIR\Contents\Info.plist"
	Delete "$INSTDIR\Contents\DefaultPrefs.json"
	RmDir "$INSTDIR\Contents"
 
	# Always delete uninstaller as the last action
	Delete "$INSTDIR\uninstall.exe"
 
	# Try to remove the install directory - this will only happen if it is empty
	RmDir $INSTDIR

	# Remove uninstaller information from the registry
	DeleteRegKey HKLM "${ARP}"

	# Restart Plex Media Server
	Call un.PlexRefreshChannelsIfLoggedIn
SectionEnd
