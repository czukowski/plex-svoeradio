!define BUILD_NUMBER_HEADER "buildnumber.nsh"
!include /NonFatal "${BUILD_NUMBER_HEADER}"

!ifndef BUILD_NUMBER
	!define BUILD_NUMBER 0 # If we have no previous number.
!else
	!delfile "${BUILD_NUMBER_HEADER}"
!endif

!define /math NEXT_BUILD_NUMBER ${BUILD_NUMBER} + 1
!appendfile "${BUILD_NUMBER_HEADER}" "!define BUILD_NUMBER ${NEXT_BUILD_NUMBER}"
