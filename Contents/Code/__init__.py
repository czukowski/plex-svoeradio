# -*- coding: utf-8 -*-
"""
    SvoeRadio.Fm plugin for Plex Media Server
    https://bitbucket.org/czukowski/plex-svoeradio/
    Copyright: 2014, Korney Czukowski
    License: MIT
"""
from locale import L, SetAvailableLanguages
import cerealizer

PREFIX = "/music/svoeradio"
NAME = "SvoeRadio.Fm"
ICON = "logo.jpg"
BACKGROUND = "back_22.jpg"


# Channel initialization
def Start():
    ObjectContainer.title1 = L(NAME)
    NextPageObject.art = R(BACKGROUND)
    DirectoryObject.art = R(BACKGROUND)
    InputDirectoryObject.art = R(BACKGROUND)
    HTTP.Headers['User-Agent'] = 'Plex Media Server/%s (%s; %s; %s)' % (Platform.ServerVersion, Platform.OS, Platform.CPU, Plugin.Identifier)
    SetAvailableLanguages(['ru'])


# Main menu
# Lists options to choose from
@handler(PREFIX, NAME, thumb=ICON, art=BACKGROUND)
def MainMenu():
    menu = ObjectContainer()
    live_stream = SharedCodeService.svoeradio.get_live_stream_request(Prefs['live_stream'])
    menu.add(CreateTrackObject(
        type=live_stream.name,
        title="%s (%s)" % (L('Online Broadcasting'), Prefs['live_stream']),
        summary=L('Listen to live broadcast'),
        bg=R(BACKGROUND),
        **live_stream.arguments
    ))
    menu.add(DirectoryObject(
        key=Callback(NowPlayingMenu),
        title=L('Now Playing'),
        summary=L('Recently played tracks'),
        thumb=R(ICON)
    ))
    menu.add(DirectoryObject(
        key=Callback(LatestMenu),
        title=L('Latest'),
        summary=L('Tracks recently added to the rotation'),
        thumb=R(ICON)
    ))
    menu.add(DirectoryObject(
        key=Callback(ArtistsMenu),
        title=L('Artists'),
        summary=L('Rotation artists directory'),
        thumb=R(ICON)
    ))
    menu.add(DirectoryObject(
        key=Callback(AudioArchiveProgramsMenu),
        title=L('Audio Archive'),
        summary=L('Listen to archived program recordings'),
        thumb=R(ICON)
    ))
    menu.add(DirectoryObject(
        key=Callback(VideoArchiveMenu),
        title=L('Video Archive'),
        summary=L('Watch archived program recordings'),
        thumb=R(ICON)
    ))
    menu.add(InputDirectoryObject(
        key=Callback(BroadcastHistoryMenu),
        title=L('Playlist History'),
        summary=L('See what\'s been played at the specific date and time'),
        thumb=R(ICON)
    ))
    menu.add(PrefsObject(
        title=L('Preferences'),
        summary=L('Choose live stream format and bitrate'),
        art=R(BACKGROUND),
        thumb=R(ICON)
    ))
    return menu


# Now Playing menu
# Lists artists to choose from
@route(PREFIX+"/now-playing")
@route(PREFIX+"/now-playing/{before}")
def NowPlayingMenu(before=None):
    menu = ObjectContainer(title2=L('Now Playing'))
    playing = SharedCodeService.svoeradio.get_now_playing(before=before)
    for key, track in playing['tracks'].items():
        menu.add(CreateTrackObject(
            type=track['request'].name,
            title='%s - %s (%s)' % (track['artist'], track['title'], track['time']),
            artist=track['artist'],
            summary=track['summary'],
            thumb=Callback(TrackThumbnail, key=key),
            **track['request'].arguments
        ))
    if playing['next_page']:
        menu.add(NextPageObject(
            key=Callback(NowPlayingMenu, before=playing['next_page']),
            title=L('Earlier...'),
            summary=L('Display earlier tracks')
        ))
    return menu


# Latest additions menu
# Lists recently added tracks to choose from
@route(PREFIX+"/latest")
@route(PREFIX+"/latest/{page}")
def LatestMenu(page=None):
    menu = ObjectContainer(title2=L('Latest'))
    latest = SharedCodeService.svoeradio.get_latest_list(page=page)
    for key, track in latest['objects'].items():
        menu.add(TrackObject(
            url=track['href'],
            title='%s (%s)' % (track['title'], track['since']),
            artist=track['artist'],
            summary='%s (%s)' % (track['title'], track['since']),
            thumb=track['image']
        ))
    if 'next_page' in latest:
        menu.add(NextPageObject(
            key=Callback(LatestMenu, page=latest['next_page']),
            summary=L('Display next page')
        ))
    return menu


# Artists menu
# Lists artists to choose from
@route(PREFIX+"/artists")
@route(PREFIX+"/artists/{page}")
def ArtistsMenu(page=None):
    menu = ObjectContainer(title2=L('Artists'))
    artists = SharedCodeService.svoeradio.get_artists(page=page)
    for key, artist in artists['objects'].items():
        menu.add(DirectoryObject(
            key=Callback(ArtistPage, key=key),
            title=artist['name'],
            summary=L('Display artist\'s tracks in rotation'),
            thumb=R(ICON)
        ))
    if artists['next_page']:
        menu.add(NextPageObject(
            key=Callback(ArtistsMenu, page=artists['next_page']),
            summary=L('Display next page')
        ))
    return menu


# Artist's tracks
# Lists tracks to choose from
@route(PREFIX+"/artist/{key}")
def ArtistPage(key):
    artist = SharedCodeService.svoeradio.get_artist_info(key)
    menu = ObjectContainer(title2=artist['name'])
    for key, track in artist['tracks'].items():
        menu.add(CreateTrackObject(
            type=track['request'].name,
            title='%s - %s (%s)' % (track['artist'], track['title'], track['time']),
            artist=track['artist'],
            summary=artist['summary'],
            thumb=artist['image'],
            **track['request'].arguments
        ))
    return menu


# Audio archive programs menu
# Lists archived programs to choose from
@route(PREFIX+"/audio-archive")
def AudioArchiveProgramsMenu():
    menu = ObjectContainer(title2=L('Audio Archive'))
    categories = SharedCodeService.svoeradio.get_audio_archive_categories()
    for key, category in categories.items():
        menu.add(DirectoryObject(
            key=Callback(AudioArchiveMenu, category=key),
            title=category['title'],
            summary=category['title'],
            thumb=R(ICON)
        ))
    menu.add(DirectoryObject(
        key=Callback(AudioArchiveMenu, category='-'),
        title=L('All Programs'),
        summary=L('All Programs'),
        thumb=R(ICON)
    ))
    return menu


# Audio archive menu
# Lists archive program recordings to choose from
@route(PREFIX+"/audio-archive/{category}")
@route(PREFIX+"/audio-archive/{category}/{page}")
def AudioArchiveMenu(category, page=None):
    programs = SharedCodeService.svoeradio.get_audio_archive_programs(category=category if category != '-' else '', page=page)
    menu = ObjectContainer(title2=programs['title'] if 'title' in programs else L('Audio Archive'))
    for key, program in programs['objects'].items():
        date = '%s %s %s' % (program['date'], program['month'], program['year'])
        menu.add(TrackObject(
            url=program['href'],
            title='%s (%s)' % (program['artist'], date),
            artist=program['artist'],
            summary='%s: %s %s' % (date, program['artist'], program['program']),
            thumb=program['image']
        ))
    if programs['next_page']:
        menu.add(NextPageObject(
            key=Callback(AudioArchiveMenu, category=category, page=programs['next_page']),
            summary=L('Display next page')
        ))
    return menu


# Video archive menu
# Lists archive program recordings to choose from
@route(PREFIX+"/video-archive")
@route(PREFIX+"/video-archive/{page}")
def VideoArchiveMenu(page=None):
    menu = ObjectContainer(title2=L('Video Archive'))
    programs = SharedCodeService.svoeradio.get_video_archive_programs(page=page)
    for key, program in programs['objects'].items():
        date = '%s %s %s' % (program['date'], program['month'], program['year'])
        menu.add(VideoClipObject(
            url=program['href'],
            title='%s (%s)' % (program['artist'], date),
            summary='%s: %s %s' % (date, program['artist'], program['program']),
            thumb=program['image']
        ))
    if programs['next_page']:
        menu.add(NextPageObject(
            key=Callback(VideoArchiveMenu, page=programs['next_page']),
            summary=L('Display next page')
        ))
    return menu


# Broadcast history menu
# Lists playlist at the date and time specified in query.
# Must have default value for search parameter, else it woudln't work.
@route(PREFIX+"/air-history")
def BroadcastHistoryMenu(query=''):
    before = SharedCodeService.svoeradio.get_before_from_query(query)
    menu = ObjectContainer(title2=L('Playlist History'))
    menu.add(InputDirectoryObject(
        key=Callback(BroadcastHistoryMenu),
        title=L('New search'),
        summary=L('See what\'s been played at the specific date and time'),
        thumb=R('Search.png')
    ))
    menu.extend(NowPlayingMenu(before=before))
    return menu


# Create track object. Adapted from Plex Shoutcast plugin.
# author: Plex Plug-ins
# see: https://github.com/plexinc-plugins/Shoutcast.bundle
@route(PREFIX+"/track")
def CreateTrackObject(type, title=None, summary=None, artist=None, bg=None, thumb=None,
                      include_container=False, **kwargs):
    request_obj = SharedCodeService.svoeradio.create_play_request(type, kwargs)
    track_object = TrackObject(
        key=Callback(CreateTrackObject, type=type, title=title, artist=artist, bg=bg, thumb=thumb,
                     include_container=True, **kwargs),
        rating_key=request_obj.rating_key,
        title=title or '?',
        summary=summary or '',
        artist=artist,
        art=bg,
        thumb=thumb or R(ICON),
        items=[
            MediaObject(
                parts=[PartObject(key=Callback(PlayAudio, ext=request_obj.ext, type=type, **kwargs))],
                container=request_obj.container,
                audio_codec=request_obj.audio_codec,
                bitrate=request_obj.bitrate,
                audio_channels=2
            )
        ]
    )
    return ObjectContainer(objects=[track_object]) if include_container else track_object


# Retrieve thumbnail for track.
@route(PREFIX+"/thumbnail/{key}")
def TrackThumbnail(key):
    thumbnail = SharedCodeService.svoeradio.get_track_image(key)
    return thumbnail or R(ICON)


# Redirect to url. Must have extension parameter so that Plex Home Theater can play it.
# PHT relies on file extension in the URL to find the correct decoder.
@route(PREFIX+"/play/{request}.{ext}")
def PlayAudio(type, **kwargs):
    request_obj = SharedCodeService.svoeradio.create_play_request(type, kwargs)
    media_url = request_obj.url
    Log('Playing \'%s\'' % media_url)
    return Redirect(media_url)
