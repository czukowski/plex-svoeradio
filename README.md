Своё радио для Plex (неофициальный плагин)
==========================================

[![Скриншот из Plex Home Theater][thumbnail]][screenshot]

Этот плагин добавляет в ваш [Plex Media Server][plex] поддержку [Своего радио][svoeradio].

Установка и обновление
----------------------

 * С помощью программы установки (для Windows): скачайте свежий установщик со [страницы загрузок][download-page]
   и запустите его. Путь установки определится автоматически. Также присутствует возможность автоматически перезапустить
   программу-сервер. Удалить плагин можно штатными средствами Windows через панель управления. 
   Примечание: программа установки без цифровой подписи.
 * Установка самостоятельно (для всех операционных систем): откройте папку, где в Plex установлены плагины (список путей к папкам,
   в зависимости от операционной системы, есть в [этой инструкции][plex-plugins-dir]) и создайте в ней папку `SvoeRadioFm.bundle`.
   Затем установите плагин в папку `SvoeRadioFm.bundle` одним из следующих способов:
    * Простой способ: скачайте архив ([zip][download-zip], [gz][download-gz] или [bz2][download-bz2]) и распакуйте его.
      Появится папка с названием похожим на `czukowski-plex-svoeradio-98b14aa39adc`. Ее содержимое (то есть, не ее саму!)
      перенесите в созданную папку `SvoeRadioFm.bundle`.
      Обновление впоследствие производите по такой же схеме, заменяйте существующие файлы новыми, от последней версии.
    * Продвинутый способ, с помощью [git][git]: перейдите в папку `SvoeRadioFm.bundle`, откройте командную строку и выполните команду:
      `git clone https://bitbucket.org/czukowski/plex-svoeradio.git .` (пробел и точка в конце)  
      Для обновления в будущем достаточно выполить команду, находясь в папке `SvoeRadioFm.bundle`:  
      `git pull`

Если вы не воспользовались возможностью программы установки автоматически перезагрузить Plex Media Server, или же произвели установку
самостоятельно, сделайте это вручную после окончания установки.

Примечания
----------

Все данные плагин получает с сайта svoeradio.fm. Сайт, как и само радио, постоянно развивается. При внесении
существенных изменений в сайт, плагин может перестать работать. Так как это неофициальный плагин, его автор не получает
информацию о таких изменениях заранее. Если плагин перестал работать, необходимо либо дождаться исправления плагина
и затем обновить его, либо, для опытных, исправить его самостоятельно и отослать автору «pull request».

Плагин распространяется по [лицензии MIT][mit].

[svoeradio]: https://svoeradio.fm/
[plex]: https://plex.tv/
[plex-plugins-dir]: https://support.plex.tv/hc/en-us/articles/201106098-How-do-I-find-the-Plug-Ins-folder-
[git]: http://git-scm.com/
[mit]: https://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D1%86%D0%B5%D0%BD%D0%B7%D0%B8%D1%8F_MIT#.D0.A2.D0.B5.D0.BA.D1.81.D1.82_.D0.BB.D0.B8.D1.86.D0.B5.D0.BD.D0.B7.D0.B8.D0.B8
[download-page]: https://bitbucket.org/czukowski/plex-svoeradio/downloads
[download-zip]: https://bitbucket.org/czukowski/plex-svoeradio/get/master.zip
[download-gz]: https://bitbucket.org/czukowski/plex-svoeradio/get/master.tar.gz
[download-bz2]: https://bitbucket.org/czukowski/plex-svoeradio/get/master.tar.bz2
[screenshot]: https://bytebucket.org/czukowski/plex-svoeradio/raw/37f06d27d75432741a270b923c055774043a486a/Screenshots/PHT_big.jpg
[thumbnail]: https://bytebucket.org/czukowski/plex-svoeradio/raw/37f06d27d75432741a270b923c055774043a486a/Screenshots/PHT_thumb.jpg